# CcDesktop

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:6200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Login and users

After login, user credentials are stored in localStorage variable `cc-desktop-user`. By default, application has 3 users that can be used to login into application. 

email: `theflash@email.com`, password: `123456`

email: `bugsbunny@email.com`, password: `123456`

email: `iamyourfather@email.com`, password: `123456`

## Caching

After first load, `games` API is called to store games inside localStorage under variable `cc-desktop-games`. This is later used to filter search result by certain query.

Pages are also cached after first time and each page is saved in localStorage under variable `cc-desktop-category-{category-name}` where `category name` presents slug of category that has been saved, i.e. `video-slots`

import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, RouterLink, RouterLinkActive } from '@angular/router';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SearchComponent } from './components/search/search.component';
import { GameComponent } from './components/game/game.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  declarations: [
    PageNotFoundComponent,
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
    SearchComponent,
    GameComponent
  ],
  providers: [],
  exports: [
    PageNotFoundComponent,
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
    SearchComponent,
    GameComponent
  ]
})
export class SharedModule { }

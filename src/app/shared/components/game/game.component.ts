import { HelperService } from './../../../services/helper.service';
import { Component, Input } from '@angular/core';
import Game from '../../../models/Game';

@Component({
  selector: 'cc-game',
  templateUrl: './game.component.html'
})
export class GameComponent {

  @Input() game: Game;

  constructor(private helperService: HelperService) { }

}

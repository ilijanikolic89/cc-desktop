import { Component, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'cc-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  @ViewChild('searchBox') searchBox: ElementRef;
  @ViewChild('searchTrigger') searchTrigger: ElementRef;

  searchQuery = '';
  isSearchVisible = false;

  @HostListener('document:click', ['$event'])
  onDocumentClick(e) {
    if (!(this.searchBox.nativeElement).contains(e.target) && this.searchTrigger.nativeElement !== e.target) {
      this.isSearchVisible = false;
    }
  }

  constructor(
    private router: Router,
    private toastr: ToastrService
  ) { }

  search() {
    if (this.searchQuery !== '') {
      if (this.searchQuery.length > 2) {
        this.router.navigate([`/search/${this.searchQuery}`]);
        this.searchQuery = '';
      }
      else {
        this.toastr.error('Search query needs to be at least 3 characters long!');
      }
    }
  }

  toggleSearch() {
    this.isSearchVisible = !this.isSearchVisible;
  }

}

import { Component, OnInit, ViewChild, TemplateRef, ElementRef, HostListener } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { User } from '../../../models/User';

@Component({
  selector: 'cc-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  activeUser: any;
  modalRef: BsModalRef;
  isAccountActive = false;

  login: object = {
    email: '',
    password: ''
  };

  @ViewChild('account') account: ElementRef;
  @ViewChild('accountTrigger') accountTrigger: ElementRef;

  @HostListener('document:click', ['$event'])
  onDocumentClick(e) {
    if (!(this.account.nativeElement).contains(e.target) && !(this.accountTrigger.nativeElement).contains(e.target)) {
      this.isAccountActive = false;
    }
  }

  constructor(
    private modalService: BsModalService,
    private userService: UserService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    if (localStorage.getItem('cc-desktop-user')) {
      this.activeUser = JSON.parse(localStorage.getItem('cc-desktop-user'));
    }
    else {
      this.activeUser = false;
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  handleLogin() {
    const tryLogin = this.userService.login(this.login);

    if (!tryLogin) {
      this.toastr.error('Wrong Credentials!');
    }
    else {
      this.modalRef.hide();
      this.toastr.success('Login successful!');
      this.login = {
        email: '',
        password: ''
      };
      setTimeout(() => {
        this.activeUser = JSON.parse(localStorage.getItem('cc-desktop-user'));
      }, 1000);
    }
  }

  handleLogout() {
    localStorage.removeItem('cc-desktop-user');
    this.toastr.info('You have been logged out');
    this.activeUser = false;
  }

  toggleAccount() {
    this.isAccountActive = !this.isAccountActive;
  }
}

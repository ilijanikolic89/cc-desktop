import { Component, OnDestroy, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Subscription } from 'rxjs/Subscription';
import { GameCategory } from '../../../models/gameCategory';

@Component({
  selector: 'cc-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  @ViewChild('menu') menu: ElementRef;
  @ViewChild('menuTrigger') menuTrigger: ElementRef;

  pages: Array<GameCategory>;
  isMenuVisible = false;

  @HostListener('document:click', ['$event'])
  onDocumentClick(e) {
    if (!(this.menu.nativeElement).contains(e.target) && !(this.menuTrigger.nativeElement).contains(e.target)) {
      this.isMenuVisible = false;
    }
  }

  constructor(private apiService: ApiService) {
    this.pages = [
      {name: 'Popular Games', slug: 'popular-games'},
      {name: 'Video Slots',  slug: 'video-slots'},
      {name: 'Table Games', slug: 'table-games'},
      {name: 'Video Poker',  slug: 'video-poker'},
      {name: 'Classic Slots', slug: 'classic-slots'},
      {name: 'Jackpot Games', slug: 'jackpot-games'},
      {name: 'Other Games', slug: 'other-games'}
    ];
  }

  toggleMenu() {
    this.isMenuVisible = !this.isMenuVisible;
  }
}

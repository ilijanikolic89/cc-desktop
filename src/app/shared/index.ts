export { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
export { HeaderComponent } from './components/header/header.component';
export { FooterComponent } from './components/footer/footer.component';
export { SearchComponent } from './components/search/search.component';
export { GameComponent } from './components/game/game.component';
export { NavigationComponent } from './components/navigation/navigation.component';

export * from './shared.module';

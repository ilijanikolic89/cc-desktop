import { Component } from '@angular/core';

@Component({
  selector: 'cc-layout',
  templateUrl: './basic-layout.component.html',
  styleUrls: ['./basic-layout.component.scss']
})
export class BasicLayoutComponent {}

import { BasicLayoutComponent } from './shared/layouts/basic-layout/basic-layout.component';
import { Route } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CategoryComponent } from './components/category/category.component';
import { SingleGameComponent } from './components/single-game/single-game.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

export const routes: Route[] = [
    {
        path: '', component: BasicLayoutComponent,
        children: [
            {
                path: '',
                component: HomeComponent
            },
            {
                path: '404',
                component: PageNotFoundComponent
            },
            {
                path: 'categories/:category',
                component: CategoryComponent
            },
            {
                path: 'search/:query',
                component: SearchResultsComponent
            },
        ],
    },
    {
        path: 'games/:gameID',
        component: SingleGameComponent
    },
    { path: '**', redirectTo: '/404' }

];

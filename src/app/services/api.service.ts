import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { API } from '../constants';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import Game from '../models/Game';


@Injectable()
export class ApiService {
  games: Observable<Game[]>;
  headers: Headers;
  apiParams: URLSearchParams;

  gamesAreLoaded: EventEmitter<any> = new EventEmitter();

  constructor(private http: Http) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');

    this.apiParams = new URLSearchParams();
    this.apiParams.append('brand', 'cherrycasino.desktop');
    this.apiParams.append('locale', 'en');

    if (localStorage.getItem('cc-desktop-games')) {
      this.games = JSON.parse(localStorage.getItem('cc-desktop-games'));
    }
    else {
      this.getAllGames().subscribe(result => {
        this.games = result;
        this.gamesAreLoaded.emit();
        localStorage.setItem('cc-desktop-games', JSON.stringify(this.games));
      });
    }
  }

  getCategory(slug): Observable<any> {
    return this.http.get(`${API}/game-categories/${slug}`, { headers: this.headers, params: this.apiParams })
      .map((response: Response) => response.json())
      .catch((error: Error) => Observable.throw('Server error'));
  }

  getGame(gameID): Observable<any> {
    return this.http.get(`${API}/games/${gameID}`, { headers: this.headers, params: this.apiParams })
      .map((response: Response) => response.json())
      .catch((error: Error) => Observable.throw('Server error'));
  }

  getAllGames(): Observable<any> {
    if (!this.games) {
      this.games = this.http.get(`${API}/games`, { headers: this.headers, params: this.apiParams })
        .map((response: Response) => response.json()._embedded.games)
        .publishReplay(1)
        .refCount()
        .catch((error: Error) => Observable.throw('Server error'));
    }

    return this.games;
  }
}

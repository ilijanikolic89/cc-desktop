import { Injectable } from '@angular/core';

@Injectable()
export class HelperService {
  updateUrl(event) {
    event.target.src = 'assets/images/placeholder.jpg';
  }
}


import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { User } from '../models/User';

@Injectable()
export class UserService {
  currentUser: User;
  users: Array<User> = [];

  constructor() {
    this.users.push(new User('Barry', 'Alen', 'theflash@email.com', '123456', 5417));
    this.users.push(new User('Bugs', 'Bunny', 'bugsbunny@email.com', '123456', 11645));
    this.users.push(new User('Darth', 'Vader', 'iamyourfather@email.com', '123456', 3026));
  }

  login(userData) {
    const user = _.find(this.users, {'email': userData.email, 'password': userData.password});
    if (user) {
      this.currentUser = user;
      localStorage.setItem('cc-desktop-user', JSON.stringify(this.currentUser));
      return user;
    }
    return false;
  }

}

import * as _ from 'lodash';
import { Component, HostListener } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import Game from '../../models/Game';
import { ApiService } from '../../services/api.service';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'cc-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent {
  results: Array<Game> = [];
  searchQuery: string;
  isLoading = true;

  constructor(
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.searchQuery = this.route.snapshot.paramMap.get('query');
        this.isLoading = true;
        this.filterGames();
      }
    });
  }

  filterGames() {
    this.results = _.filter(this.apiService.games, (game) => {
      const name = (game.name).toLowerCase();
      return _.includes(name, this.searchQuery);
    });
    this.isLoading = false;
  }
}

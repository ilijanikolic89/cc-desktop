import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GameCategory } from '../../models/gameCategory';
import { ApiService } from '../../services/api.service';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'cc-category',
  templateUrl: './category.component.html'
})
export class CategoryComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  category = new GameCategory();
  currentPage: string;
  isLoading = true;

  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentPage = this.route.snapshot.paramMap.get('category');
        this.isLoading = true;
        this.getCategory();
      }
    });
  }

  ngOnInit() {}

  getCategory() {
    if (localStorage.getItem(`cc-desktop-category-${this.currentPage}`)) {
      this.category = JSON.parse(localStorage.getItem(`cc-desktop-category-${this.currentPage}`));
      this.isLoading = false;
    }
    else {
      this.subscription = this.apiService.getCategory(this.currentPage).subscribe(
        (data) => {
          this.category = {
            name: data.name,
            slug: data.slug,
            games: data._embedded.games
          };
          setTimeout(() => {
            localStorage.setItem(`cc-desktop-category-${this.currentPage}`, JSON.stringify(this.category));
            this.isLoading = false;
          }, 200);
        }
      );
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

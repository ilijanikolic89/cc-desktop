import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import Game from '../../models/Game';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'cc-home',
  templateUrl: './home.component.html',
  providers: [
    { provide: CarouselConfig, useValue: { interval: 5000, noPause: true, showIndicators: false } }
  ]
})
export class HomeComponent implements OnInit {

  isLoading = false;
  popularGames: Array<Game> = [];
  newGames: Array<Game> = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.isLoading = true;
    this.apiService.gamesAreLoaded.subscribe(() => {
        this.getPopularGames();
        this.getNewGames();
        this.isLoading = false;
    });
    if (localStorage.getItem('cc-desktop-games')) {
      this.getPopularGames();
      this.getNewGames();
      this.isLoading = false;
    }
  }

  getPopularGames() {
    this.popularGames = _.filter(this.apiService.games, (game) => {
      return game.rating >= 1;
    });
    this.popularGames = _.take((_.orderBy(this.popularGames, ['rating'], ['desc'])), 12);
  }

  getNewGames() {
    const today = moment().format('YYYY');
    this.newGames = _.filter(this.apiService.games, (game) => {
      return _.includes(game.created_at.date, today);
    });
    this.newGames = _.take((_.orderBy(this.newGames, ['created_at.date'], ['desc'])), 6);
    console.log(this.newGames);
  }

}

import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, AfterViewInit, OnDestroy, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import Game from '../../models/Game';

@Component({
  selector: 'cc-single-game',
  templateUrl: './single-game.component.html',
  styleUrls: ['./single-game.component.scss']
})
export class SingleGameComponent implements OnInit, OnDestroy, AfterViewInit {
  modalRef: BsModalRef;
  subscription: Subscription;
  singleGame = new Game();
  gameID: string;
  isLoading = true;
  isGameLoaded = false;

  constructor(
    private modalService: BsModalService,
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.gameID = this.route.snapshot.paramMap.get('gameID');
    this.subscription = this.apiService.getGame(this.gameID).subscribe(
      (data) => {
        this.singleGame = data;
        setTimeout(() => {
          this.isLoading = false;
        }, 200);
      }
    );
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.isGameLoaded = true;
    }, 5000);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  exitGame() {
    this.modalRef.hide();
    this.router.navigate(['/']);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

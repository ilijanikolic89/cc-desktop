import { UserService } from './services/user.service';
import { HelperService } from './services/helper.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { RouterModule, RouterLink, RouterLinkActive } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { FormsModule, NgModel } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SharedModule } from './shared';

import { BasicLayoutComponent } from './shared/layouts/basic-layout/basic-layout.component';

import { AppComponent } from './app.component';

import { routes } from './app.routes';
import { ApiService } from './services/api.service';
import { CategoryComponent } from './components/category/category.component';
import { HomeComponent } from './components/home/home.component';
import { SingleGameComponent } from './components/single-game/single-game.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';



@NgModule({
  declarations: [
    AppComponent,
    BasicLayoutComponent,
    CategoryComponent,
    HomeComponent,
    SingleGameComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpModule,
    FormsModule,
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 1500,
      preventDuplicates: true
    }),
    RouterModule.forRoot(
      routes,
      { enableTracing: false }
   ),
  ],
  providers: [HttpModule, ApiService, HelperService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }

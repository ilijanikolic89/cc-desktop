import Game from './Game';

export class GameCategory {
    name: string;
    slug: string;
    games?: Array<Game>;

    constructor(name: string = '', slug: string = '', games?: Array<Game>) {
        this.name = name;
        this.slug = slug;
        this.games = games;
    }
}

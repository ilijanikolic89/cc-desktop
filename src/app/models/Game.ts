export default class Game {
    backgrounds: Array<string>;
    name: string;

    constructor(name: string = '', backgrounds: Array<string> = ['']) {
        this.name = name;
        this.backgrounds = backgrounds;
    }
}

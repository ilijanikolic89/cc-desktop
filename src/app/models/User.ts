export class User {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    balance: number;

    constructor(firstName: string, lastName: string, email: string, password: string, balance: number) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.balance = balance;
    }
}
